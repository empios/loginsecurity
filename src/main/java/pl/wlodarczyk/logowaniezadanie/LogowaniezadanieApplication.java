package pl.wlodarczyk.logowaniezadanie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogowaniezadanieApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogowaniezadanieApplication.class, args);
    }

}
