package pl.wlodarczyk.logowaniezadanie.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {


    @RequestMapping("/forUser")
    public String forUser(){
        return "user";
    }
    @RequestMapping("/login")
    public String login(){
        return "login";
    }

}
